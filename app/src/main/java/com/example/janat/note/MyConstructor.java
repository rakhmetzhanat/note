package com.example.janat.note;

/**
 * Created by Janat on 07.03.2016.
 */
public class MyConstructor {
    String text;
    String date;
    String id;

    public MyConstructor(String date, String text, String id) {
        this.date = date;
        this.text = text;
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
