package com.example.janat.note;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Janat on 07.03.2016.
 */
public class MyAdapter extends ArrayAdapter<MyConstructor>{

    DBHelper dbHelper;
    SQLiteDatabase db;
    ArrayList<MyConstructor> adapter;
    ImageButton imageButton, imageButton2;
    ListView myListView;
    Animation animation;
    Thread thread;

    public MyAdapter(Context context, ArrayList<MyConstructor> notes, ListView listView) {
        super(context, R.layout.list_item, notes);
        adapter = notes;
        myListView = listView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {

        final LayoutInflater inflater = LayoutInflater.from(getContext());

        final View customView = inflater.inflate(R.layout.list_item, parent, false);
        final TextView date = (TextView) customView.findViewById(R.id.textView);
        final TextView text = (TextView) customView.findViewById(R.id.textView2);
        imageButton = (ImageButton) customView.findViewById(R.id.imageButton);
        imageButton2 = (ImageButton) customView.findViewById(R.id.imageButton2);

        date.setText(adapter.get(position).getDate());
        text.setText(adapter.get(position).getText());

        dbHelper = new DBHelper(this.getContext());
        db = dbHelper.getWritableDatabase();

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                text.setBackgroundColor();
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                customView.startAnimation(animation);

                final Cursor cursor = db.query("mytable", null, null, null, null, null, null);
                int delCount = db.delete("mytable", "id = " + adapter.get(position).getId(), null);
                adapter.removeAll(adapter);
                int id = cursor.getColumnIndex("id");
                int date = cursor.getColumnIndex("date");
                int text = cursor.getColumnIndex("text");
                if(cursor.moveToFirst()){
                    do {
                        adapter.add(new MyConstructor(cursor.getString(date), cursor.getString(text), cursor.getString(id)));
                    } while (cursor.moveToNext());
                }
                myListView.setAdapter(new MyAdapter(getContext(), adapter, myListView));

            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialogDetails = null;
                LayoutInflater inflater = LayoutInflater.from(getContext());
                final View dialogview = inflater.inflate(R.layout.note, null);
                final AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(getContext());
                dialogbuilder.setView(dialogview);
                dialogDetails = dialogbuilder.create();
                final EditText editText = (EditText) dialogview.findViewById(R.id.editText);
                editText.setText(adapter.get(position).getText());

                dialogbuilder.setPositiveButton("Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Cursor cursor = db.query("mytable", null, null, null, null, null, null);
                                ContentValues cv = new ContentValues();

                                String text = editText.getText().toString();

                                if(!text.equals("") && !text.equals(null) && text != "") {
                                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                                    cv.put("text", text);

                                    int updCount = db.update("mytable", cv, "id" + "=" + adapter.get(position).getId(), null);
                                    Toast.makeText(getContext(), "Modified Successfully", Toast.LENGTH_SHORT).show();

                                    adapter.removeAll(adapter);
                                    int id = cursor.getColumnIndex("id");
                                    int date = cursor.getColumnIndex("date");
                                    int title = cursor.getColumnIndex("text");

                                    if(cursor.moveToFirst()){
                                        do {
                                            adapter.add(new MyConstructor(cursor.getString(date), cursor.getString(title), cursor.getString(id)));
                                        } while (cursor.moveToNext());

                                    }
                                    myListView.setAdapter(new MyAdapter(getContext(), adapter, myListView));

                                }else
                                {

                                }

                            }
                        });
                dialogbuilder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                dialogbuilder.show();
            }
        });

        return customView;
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // создаем таблицу с полями
            db.execSQL("create table mytable ("
                    + "id integer primary key autoincrement,"
                    + "text text,"
                    + "date DATETIME DEFAULT CURRENT_TIMESTAMP" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

}
