package com.example.janat.note;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    DBHelper dbHelper;
    SQLiteDatabase db;

    List<String> names = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);;
        final ArrayList<MyConstructor> notes = new ArrayList<MyConstructor>();

        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();

        Cursor cursor = db.query("mytable", null, null, null, null, null, null);

        int id = cursor.getColumnIndex("id");
        int date = cursor.getColumnIndex("date");
        int text = cursor.getColumnIndex("text");

        if(cursor.moveToFirst()){
            do {
               notes.add( new MyConstructor(cursor.getString(date), cursor.getString(text), cursor.getString(id)));
            } while (cursor.moveToNext());

        }

        ListView lvMain = (ListView) findViewById(R.id.listView);
        MyAdapter adapter = new MyAdapter(MainActivity.this, notes, lvMain);
        lvMain.setAdapter(adapter);


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog dialogDetails = null;
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                final View dialogview = inflater.inflate(R.layout.note, null);
                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(MainActivity.this);
                dialogbuilder.setView(dialogview);
                dialogDetails = dialogbuilder.create();

                dialogbuilder.setPositiveButton("Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                EditText editText = (EditText) dialogview.findViewById(R.id.editText);

                                ContentValues cv = new ContentValues();

                                String text = editText.getText().toString();

                                if(!text.equals("") && !text.equals(null) && text != "") {
                                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                                    cv.put("text",text);

                                    long rowID = db.insert("mytable", null, cv);
                                    Toast.makeText(MainActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
                                    recreate();
                                }

                            }
                        });
                dialogbuilder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                dialogbuilder.show();
            }
        });
    }
}
